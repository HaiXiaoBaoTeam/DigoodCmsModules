<?php
//+----------------------------------------------------|
// | Description: 热门产品模块
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-16
//+----------------------------------------------------|
## 验证必填参数
if(!isset($itemsData)) {
    echo "<code>参数不完整：需要传入 itemsData => array()</code>";
    return;
}

## 验证参数类型
if(!is_array($itemsData)) {
    echo "<code>参数错误：itemsData 应该为数组</code>";
    return;
}

## 设置默认参数
if(!isset($quantiy)) $quantiy = 4;
if(!isset($nav)) $nav = true;
if(!isset($title)) $title = '';
?>

<div digood-id="product_releted_1_oyh">
    <div class="related-product all-product-wrapper">
        <h5 class="title"><?=$title?></h5>
        <div class="row">
            <div class="related-product-slider owl-carousel owl-theme"  data-items="<?=$quantiy?>" data-nav="<?=intval($nav)?>">

            <?php foreach ($itemsData as $index => $item): ?>
                <?php
                    ## 验证参数
                    if(!isset($item['name']) || !isset($item['image']) || !isset($item['url'])) {
                        echo "<code>错误：请检查 itemsData 数据，数据中必须包含 name, image, url</code>";
                        return;
                    }

                    if(!isset($item['button'])) $item['button'] = 'View Detail';
                    if(!isset($item['description'])) $item['description'] = '';
                ?>
                <div class="item">
                    <div class="single-item">
                        <a href="<?=$item['url']?>" title="<?=$item['name']?>">
                            <img alt="<?=$item['name']?>" src="<?=$item['image']?>">
                        </a>
                        <h5><a href="<?=$item['url']?>" class="tran3s"><?=$item['name']?></a></h5>
                        <div class="clearfix">
                        <p class="description"><?=$item['description']?></p>
                        </div>
                        <a href="<?=$item['url']?>" class="tran3s cart"><?=$item['button']?></a>
                    </div> <!-- /.single-item -->
                </div> <!-- /.col- -->
                <?php endforeach; ?>

            </div> <!-- /.related-product-slider -->
        </div> <!-- /.row -->
    </div> <!-- /.related-product -->
</div>