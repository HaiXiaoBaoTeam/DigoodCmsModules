制作：2018年5月11日 OYH

### 数据结构演示

````

$data = [
    'itemsData' => [
        [
            'name' => 'Dapibus ac facilisis in',
            'image' => 'https://picsum.photos/500/500?image=13',
            'url' => '/product/all.html',
            'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida.',
        ],
        [
            'name' => 'Dapibus ac facilisis in',
            'image' => 'https://picsum.photos/500/500?image=16',
            'url' => '/product/all.html',
            'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida.',
        ],
        [
            'name' => 'Dapibus ac facilisis in',
            'image' => 'https://picsum.photos/500/500?image=23',
            'url' => '/product/all.html',
            'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida.',
        ],
        [
            'name' => 'Dapibus ac facilisis in',
            'image' => 'https://picsum.photos/500/500?image=43',
            'url' => '/product/all.html',
            'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida.',
        ],
    ],
    'quantiy' => 3,
    'nav' => true,
    'title' => 'Releted Products'
]; 

````

-------------------

### 必填参数：

`itemsData`：二维数组 【列表数据】

--------------------

### 选填参数：

`title` 字符串类型, 无默认值 【模块区域标题】

`quantiy`：数字类型, 默认值`4` 【展示数量，建议不要超过5个】

`nav`：布尔类型, 默认值`true` 【是否显示切换按钮】