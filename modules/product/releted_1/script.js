/** 
* | Description: 
* +---------------------------------------------------|
* | Author: 浩丶IMOYH [oyhemail@163.com]
* | Last Modified: 2018-5-16
**/
if(!$ || !$('body').owlCarousel) {
    document.write('<code>detail/product_show_1 模块需要依赖 OwlCarousel2 和 jQuery</code>');
    return;
}

var $mains = $('[digood-id="product_releted_1_oyh"]');

$mains.each(function() {
    var $main = $(this);
    var $owl = $main.find('.owl-carousel');
    var items = $owl.data('items');
    var nav = $owl.data('nav');
    $owl = $owl.owlCarousel({
        loop: true,
        margin: 10,
        nav: (nav == 1),
        navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
        dots: false,
        responsive:{
            0:{
                items: 1
            },
            480:{
                items: 2
            },
            640:{
                items: 3
            },
            960:{
                items: items
            }
        }
    })

})