
if(!$ || !$('body').owlCarousel) {
    document.write('<code>home/banner_1 模块需要依赖 OwlCarousel2 和 jQuery</code>');
    return;
}


$mains = $('[digood-id="home_banner_1_oyh"]');
$mains.each(function (index) {
    var $main = $(this);
    var $images = $main.find('.image');
    var responsive = $main.data('responsive');
        responsive[0] = responsive[0].reverse();
        responsive[1] = responsive[1].reverse();

    var width = $(window).width();
    var len = responsive[0].length;
    var height = 0;
    for (var i = 0; i < len; i++) {
        if(responsive[0][i] < width) {
            height = responsive[1][i];
        }
    }
    if(!height) height = responsive[1][0];
    $images.height(height);
    $(window).resize(function() {
        width = $(window).width();
        for (var i = 0; i < len; i++) {
            if(responsive[0][i] < width) {
                height = responsive[1][i];
            }
        }
        if(!height) height = responsive[1][0];
        $images.height(height);
    })

    $images.width('100%');
    
    var loop = ($main.data('loop') == true);
    var nav = ($main.data('nav') == true);
    var timeout = $main.data('timeout');
    var autoplay = ($main.data('autoplay') == true);

    $main.find('.owl-carousel').owlCarousel({
        margin: 0,
        items: 1,
        nav: nav,
        loop: loop,
        autoplay: autoplay,
        autoplayTimeout: timeout,
        autoplayHoverPause: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    })
})


