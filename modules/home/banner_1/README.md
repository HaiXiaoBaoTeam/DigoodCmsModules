制作：2018年5月14日 OYH

### 数据结构演示

````
$data = [
    'itemsData' => [
        [
            'image' => 'https://picsum.photos/1920/600?image=60',
            'title' => 'Hello, world!',
            'sub_title' => 'This is a simple hero unit',
            'description' => 'A simple jumbotron-style component for calling extra attention to featured content or information.',
            'link' => '/products',
            'text_align' => 'left',
        ],
        [
            'image' => 'https://picsum.photos/1920/600?image=60',
            'title' => 'Hello, world!',
            'sub_title' => 'This is a simple hero unit',
            'description' => 'A simple jumbotron-style component for calling extra attention to featured content or information.',
            'link' => '/products',
            'button_2_link' => '/index',
            'text_align' => 'center',
        ],
        [
            'image' => 'https://picsum.photos/1920/600?image=200',
            'title' => 'Hello, world!',
            'sub_title' => 'This is a simple hero unit',
            'description' => 'A simple jumbotron-style component for calling extra attention to featured content or information.',
            'link' => '/products',
            'button_2_link' => '/index',
            'text_align' => 'right',
        ]
    ],
    'button' => 'View Detail',
    'button_2' => 'View More',
    'responsive' => [
        [1200, 960, 640, 480],
        [600, 540, 480, 320],
    ],
    'animat' => 'default'
]; 
````

-------------------

### 必填参数：

`itemsData`：多维数组 【Banner列表数据】

--------------------

### 选填参数：

`button`：字符串类型, 无默认值 【默认按钮名称，不填时不显示按钮】（列表数据中存在时，使用列表数据内的值）

`button_2`：字符串类型, 无默认值 【默认按钮-2名称，不填时不显示按钮】（列表数据中存在时，使用列表数据内的值，列表数据中需要 `button_2_link`字段）

`responsive`：二维数组, 默认值，`[[1200, 960, 640, 480],[600, 540, 480, 320],]`

`animat`：字符串类型, 无默认值，可选值：`'default'` 【动画效果】

`loop`：布尔类型，默认值：`true` 【循环】

`nav`：布尔类型，默认值：`true` 【Banner 切换按钮】

`autoplay`：布尔类型，默认值：`true` 【自动播放】

`timeout`：整数类型，默认值：`8000` 【Banner切换时间】