<?php
//+----------------------------------------------------|
// | Description: Banner
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-14
//+----------------------------------------------------|

## 验证必填参数
if(!isset($itemsData)) {
    echo "<code>参数不完整：需要传入 itemsData => array()</code>";
    return;
}


## 选填参数
if(!isset($loop)) $loop = true;
if(!isset($nav)) $nav = true;
if(!isset($autoplay)) $autoplay = true;
if(!isset($timeout)) $timeout = 8000;
if(!isset($button)) $button = '';
if(!isset($button)) $button = '';
if(!isset($button_2)) $button_2 = '';
if(!isset($responsive) || count($responsive) < 2) $responsive = [
    [1200, 960, 640, 480],
    [600, 540, 480, 320],
];
?>

<div digood-id="home_banner_1_oyh"
    <?php 
        if(isset($animat)) echo "data-animat=\"{$animat}\"";
        if($loop) echo 'data-loop="true"';
        if($nav) echo 'data-nav="true"';
        if($autoplay) echo 'data-autoplay="true"';
        if($timeout) echo 'data-timeout="'.$timeout.'"';
    ?>
    data-responsive="<?=json_encode($responsive)?>">
    <div class="owl-carousel owl-theme">
        <?php foreach ($itemsData as $index => $item): ?>
            <?php 
                if(!isset($item['image'])) continue;
                if(!isset($item['text_align'])) $item['text_align'] = 'left';
                if(!isset($item['button'])) $item['button'] = $button;
                if(!isset($item['button_2'])) $item['button_2'] = $button_2;
            ?>
            <div class="item">
                <?php if($item['link'] && !$item['button']) echo "<a class=\"item-link\" href=\"{$item['link']}\" target=\"_blank\"></a>" ?>
                <div class="item-main">
                    <div class="image-wrap">
                        <div class="image" style="background-image: url(<?=$item['image']?>); height: <?=$responsive[1][0]?>px;"></div>
                    </div>
                    <div class="text-wrap text-<?=$item['text_align']?>">
                        <div class="container">
                            <div class="col-sm-12 ele-list">
                                <?php if(isset($item['title']) && $item['title']) echo "<h2 class=\"title ele\">{$item['title']}</h2>"; ?>
                                <?php if(isset($item['sub_title']) && $item['sub_title']) echo "<p class=\"sub-title ele\">{$item['sub_title']}</p>"; ?>
                                <?php if(isset($item['description']) && $item['description']) echo "<p class=\"description ele\">{$item['description']}</p>"; ?>
                                <div class="buttons ele">
                                    <?php if(isset($item['link']) && $item['link'] && $item['button']) echo "<a href=\"{$item['link']}\" target=\"_blank\" class=\"btn btn-primary\">{$item['button']}</a>";?>
                                    <?php if(isset($item['button_2_link']) && $item['button_2_link'] && $item['button_2']) echo "<a href=\"{$item['button_2_link']}\" target=\"_blank\" class=\"btn btn-default\">{$item['button_2']}</a>";?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
