<?php
//+----------------------------------------------------|
// | Description: 示例
// | 模块参数: say
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-9
//+----------------------------------------------------|


## 设置默认参数
if(!isset($action)) $say_zh = '/search/product';
?>


<div digood-id="search_btn_1_znz">
	<!--Search Box-->    
    <div class="dropdown">
        <button class="digood-search-box-btn dropdown-toggle" type="button" id="search_btn_1_znz_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
        <ul class="dropdown-menu pull-right digood-search-panel" aria-labelledby="search_btn_1_znz_dropdownMenu">
            <li class="panel-outer">
                <div class="form-container">
                    <form action="<?=$action?>">
                        <div class="form-group">
                            <input name="keyword" type="search" value="" placeholder="Search Here" required>
                            <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                        </div>
                    </form>
                </div>
            </li>
        </ul>
    </div>    
</div>