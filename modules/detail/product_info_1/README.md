制作：2018年5月11日 OYH

### 数据结构演示

````
$data = [
    'name' => 'Dapibus ac facil in dapus ac facilisis in',
    'description' => '<p>Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida.</p><p>Egestas eget quam. Donec id elit non mi porta gravida.</p><p>Facilisis in, egestas eget quam..</p>',
    'link_1' => '/contact-us'
];

````

-------------------

### 必填参数：

`name`：字符串类型 【产品标题名称】

`description`：字符串类型 【产品描述】

`link_1`：字符串类型 【按钮1 链接, 一般为询盘页面地址】

--------------------

### 选填参数：

`sub_name`：字符串类型, 无默认值 【副标题，位于标题下方】

`button_1`：字符串类型, 默认值`'Send Inquiry'` 【按钮1名称，需要 link_1 参数填写的情况下才会显示】

`button_2`：字符串类型, 默认值`'Contact US'` 【按钮2名称，需要 link_2 参数填写的情况下才会显示】

`link_2`：字符串类型, 无默认值 【按钮2 链接】

`addthis`：布尔类型, 默认值`true` 【是否开启addthis分享】