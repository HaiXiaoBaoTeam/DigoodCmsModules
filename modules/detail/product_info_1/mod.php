<?php
//+----------------------------------------------------|
// | Description: 
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-16
//+----------------------------------------------------|
if(isset($title)) $name = $title;
## 验证必填参数
if(!isset($name) || !isset($description) || !isset($link_1)) {
    echo "<code>参数不完整：需要传入 name, description, link_1</code>";
    return;
}
## 设置默认参数
if(!isset($sub_name)) $sub_name = '';
if(!isset($button_1)) $button_1 = 'Send Inquiry';
if(!isset($button_2)) $button_2 = 'Contact US';
if(!isset($link_2)) $link_2 = '';
if(!isset($addthis)) $addthis = true;
?>

<div digood-id="detail_product_info_1_oyh">
    <div class="header">
        <h1 class="title"><?=$name?></h1>
        <p class="sub-title"><?=$sub_name?></p>
    </div>
    <div class="body">
        <div class="description">
            <?=$description?>
        </div>
    </div>
    <div class="footer">
        <div class="buttons">
            <a href="<?=$link_1?>" class="btn btn-primary"><?=$button_1?></a>
            <?php if($link_2 && $button_2): ?>
            <a href="<?=$link_2?>" class="btn btn-default"><?=$button_2?></a>
            <?php endif; ?>
        </div>

        <?php if($addthis): ?>
        <div class="sharing">
            <div class="addthis_sharing_toolbox"></div>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51e0a4c703162153"></script>
        <?php endif; ?>
    </div>
</div>