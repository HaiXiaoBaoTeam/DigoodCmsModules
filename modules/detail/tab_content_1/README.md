制作：2018年5月11日 OYH

### 数据结构演示

````
$data = [
    'tabsData' => [
        [
            'nav' => 'Detail',
            'content' => '<p> tab 1 content Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida.</p><p>Egestas eget quam. Donec id elit non mi porta gravida.</p><p>Facilisis in, egestas eget quam..</p>'
        ],
        
        [
            'nav' => 'Load Module',
            'load' => ['view/include/demo', ['say' => 'hello digood tabs']]
        ]
    ]
];

````

-------------------

### 必填参数：

`tabsData`：数组类型 【选项卡数据】

--------------------

### 选填参数：