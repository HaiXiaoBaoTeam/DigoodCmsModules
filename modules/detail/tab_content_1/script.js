var $mains = $('[digood-id="detail_tab_content_1_oyh"] .tab-main');
$mains.each(function (index) { 
    var $main = $(this);
    var $navs = $main.find('.tab-nav');
    var $contents = $main.find('.tab-content');
    var tabEvent = $main.data('event');

    $navs.each(function(index) {
        $(this).on(tabEvent, function() {
            $navs.removeClass('active');
            $contents.removeClass('active');
            $(this).addClass('active');
            $contents.eq(index).addClass('active');
        })
    })
});