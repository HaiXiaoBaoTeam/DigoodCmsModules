<?php
//+----------------------------------------------------|
// | Description: 
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-16
//+----------------------------------------------------|
## 验证必填参数
if(!isset($tabsData)) {
    echo "<code>参数不完整：需要传入 tabsData => array()</code>";
    return;
}

## 验证参数类型
if(!is_array($tabsData)) {
    echo "<code>参数错误：tabsData 应该为数组</code>";
    return;
}
?>



<div digood-id="detail_tab_content_1_oyh">
    <div class="tab-main" data-event="click">
        <div class="tab-navs clearfix">
            <?php foreach ($tabsData as $index => $tab): ?>
            <?php if(isset($tabsData[$index]['content']) || isset($tabsData[$index]['load'])): ?>
            <div class="tab-nav<?php if(!$index) echo ' active';?>">
                <a href="javascript:;"><?=$tab['nav']?></a>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
        </div>

        <div class="tab-body">
            <?php foreach ($tabsData as $index => $tab): ?>
            <?php if(isset($tabsData[$index]['content']) || isset($tabsData[$index]['load'])): ?>
            <div class="tab-content<?php if(!$index) echo ' active';?>">
                <?php
                    if(isset($tab['content'])) {
                        echo $tab['content'];
                    } elseif(isset($tab['load']) && is_array($tab['load'])) {
                        self::load($tab['load'][0], $tab['load'][1]);
                    } else {
                        self::load($tab['load']);
                    }
                ?>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>