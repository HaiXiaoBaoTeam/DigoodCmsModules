/** 
* | Description: 
* +---------------------------------------------------|
* | Author: 浩丶IMOYH [oyhemail@163.com]
* | Last Modified: 2018-5-15
**/



if(!$ || !$('body').owlCarousel) {
    document.write('<code>detail/product_show_1 模块需要依赖 OwlCarousel2 和 jQuery</code>');
    return;
}
var $mains = $('[digood-id="detail_product_show_1_oyh"]');

$mains.each(function() {
    var $main = $(this);
    var $owl = $main.find('.owl-carousel')
    $owl = $owl.owlCarousel({
        loop: false,
        margin: 3,
        nav: true,
        dots: false,
        autoplay: false,
        navText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:5
            }
        }
    })

    var postion = 0;
    var $items = $owl.find('.owl-item .item');
    $items.each(function(index) {
        $(this).on('click',function() {
            var image = $(this).find('img').attr('src');
            $main.find('.main-image img').attr('src', image);
            $items.removeClass('current');
            $(this).addClass('current');
            postion = index;
        })
    })

    $main.find('.owl-next').click(function(e) {
        postion++;
        if($items.length == postion) {
            postion = $items.length - 1;
            return;
        }
        $owl.trigger('prev.owl.carousel',[0]);
        $items.removeClass('current');
        $items.eq(postion).addClass('current');
        var image = $items.eq(postion).find('img').attr('src');
        $main.find('.main-image img').attr('src', image);
        if(postion) {
            $owl.trigger('next.owl.carousel');
        }
    })

    $main.find('.owl-prev').click(function() {
        --postion;
        if(postion < 0) postion = 0;
        if(postion < $items.length - 3) {
            $owl.trigger('prev.owl.carousel');
        }
        $items.removeClass('current');
        $items.eq(postion).addClass('current');
        $main.find('.main-image img').attr('src', image);
        var image = $items.eq(postion).find('img').attr('src');
        $main.find('.main-image img').attr('src', image);
    }) 

})
