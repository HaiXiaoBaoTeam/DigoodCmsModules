<?php
//+----------------------------------------------------|
// | Description: 
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-15
//+----------------------------------------------------|
## 验证必填参数
if(!isset($images)) {
    echo "<code>参数不完整：需要传入 images => array()</code>";
    return;
}

## 验证参数类型
if(!is_array($images)) {
    echo "<code>参数错误：images 应该为数组</code>";
    return;
}



?>

<div digood-id="detail_product_show_1_oyh">
    <div class="main-image hidden-xs">
        <img src="<?=$images[0]?>" alt="">
    </div>
    <?php if(count($images) > 1): ?>
    <div class="list-image">
        <div class="owl-carousel owl-theme">
            <?php foreach ($images as $index => $image): ?>
            <div class="item<?php if($index == 0) echo ' current';?>">
                <img src="<?=$image?>">
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>
</div>

