制作：2018年5月11日 OYH

### 数据结构演示

````
$data = [
    'images' => [    
        'https://picsum.photos/500/500?image=1',
        'https://picsum.photos/500/500?image=2',
        'https://picsum.photos/500/500?image=3',
        'https://picsum.photos/500/500?image=4',
        'https://picsum.photos/500/500?image=5',
        'https://picsum.photos/500/500?image=6'
    ]
];
````

-------------------

### 必填参数：

`images`：数组类型 【展示图片组】

--------------------

### 选填参数：
