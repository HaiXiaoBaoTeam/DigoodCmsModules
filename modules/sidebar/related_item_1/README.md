制作：2018年5月11日 OYH

### 数据结构演示

````
$data = [
    'name' => 'Dapibus ac facilisis in',
    'img' => 'http://placehold.it/100x100',
    'url' => '/product/item-1.html'
];
````

-------------------

### 必填参数：

`name`：字符串类型 【标题名称，建议限制长度】

`img`：字符串类型 【图片地址，需要绝对地址】

`url`：字符串类型 【URL地址，请填写相对地址：如'/product/item-1.html'】

--------------------

### 选填参数：
