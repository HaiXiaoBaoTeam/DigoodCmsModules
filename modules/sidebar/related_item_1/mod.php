<?php
//+----------------------------------------------------|
// | Description: 侧边栏图文展示 - 1
// | 模块参数: name, img, url
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-15
//+----------------------------------------------------|
## 验证必填参数
if(!isset($itemsData)) {
    echo "<code>参数不完整：需要传入 itemsData => array()</code>";
    return;
}

## 验证参数类型
if(!is_array($itemsData)) {
    echo "<code>参数错误：itemsData 应该为数组</code>";
    return;
}
?>

    <div digood-id="sidebar_related_item_1_oyh">
        <div class="panel panel-default">
            <div class="panel-body">

                <?php foreach ($itemsData as $index => $item): ?>
                <?php if(isset($item['title'])) $item['name'] = $item['title']; ?>
                <div class="media">
                    <div class="media-left">
                        <a href="<?=$item['url']?>" class="media-link">
                            <img class="media-object" src="<?=$item['img']?>" alt="<?=$item['name']?>">
                        </a>
                    </div>
                    <div class="media-body">
                        <h5 class="media-heading">
                            <a href="<?=$item['url']?>"><?=$item['name']?></a>
                        </h5>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>