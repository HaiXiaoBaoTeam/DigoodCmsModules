<?php
//+----------------------------------------------------|
// | Description: 侧边栏导航模块
// | 模块参数: items, level, list_id => 'digood-module-sidebar-item', wrap_id => 'digood-module-sidebar-main'
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-16
//+----------------------------------------------------|

## 验证必填参数
if(!isset($items)) {
    echo "<code>参数不完整：必须传入 items , items 是一个具有层级关系的数组</code>";
    return;
}
## 设置默认参数
if(!isset($level)) $level = 5;
if(!isset($list_id)) $list_id = 'digood-module-sidebar-item';
if(!isset($wrap_id)) $wrap_id = 'digood-module-sidebar-main';
if(!isset($deep)) $deep = 1;
?>

<div digood-id="sidebar_nav_1_oyh">
    <?php init($items, $level, $list_id, $wrap_id, $deep); ?>
</div>

<?php function init($items, $level=5, $list_id='digood-module-sidebar-item', $children_id='digood-module-sidebar-main', $deep = 1) { ?>
    <?php if($level && $level < $deep) return;?>
    <ul class="panel-group<?php if($deep > 1) echo ' collapse'?>" role="tablist" id="<?=$children_id?>">
        <?php foreach ($items as $index => $item):
            ## 判断必要参数
            if(!isset($item['name']) || !isset($item['url'])) {
                echo "<code>参数错误：items 中必须包含 name, url</code>";
                return;
            }
            ## 初始化
            $n = $index + 1;
            $item_id = "{$list_id}-{$n}";
            $child_id = "{$list_id}-{$n}-children";
            $has_children = isset($item['children']) && is_array($item['children']) && count($item['children']);
        ?>
        <li class="panel panel-default" id="<?=$item_id?>">
            <div class="panel-heading" role="tab">
                <a class="collapse-link" href="<?=$item['url']?>"><?=Lang($item['name'])?></a>
                <?php if($has_children && ( $level && $level > $deep )): ?>
                <a class="collapse-button collapsed" role="button" data-toggle="collapse" data-parent="#<?=$item_id?>" href="#<?=$child_id?>" aria-controls="<?=$child_id?>">
                    <span class="text-right">
                        <i class="fa fa-angle-down"></i>
                    </span>
                </a>
                <?php endif; ?>
            </div>
            <?php 
                ## 遍历子级
                if($has_children) init($item['children'], $level, $item_id, $child_id, $deep + 1);
            ?>
        </li>
        <?php endforeach; ?>
    </ul>
<?php } ?>






