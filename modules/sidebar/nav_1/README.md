制作：2018年5月11日 OYH

### 数据结构演示

````
$data = [
    'items' => [
        [
            'name' => 'SideBar Nav menu',
            'url'  => '/product/sidebar-nav-menu.html',
            'children' => [
                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html',
                    'children' => [
                        [
                            'name' => 'SideBar Nav sub child menu',
                            'url'  => '/product/sidebar-nav-menu.html'
                        ],
                        
                        [
                            'name' => 'SideBar Nav sub child menu',
                            'url'  => '/product/sidebar-nav-menu.html'
                        ] 
                    ],
                ],
                
                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html'
                ],
                
                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html'
                ]
            ],
        ],
        [
            'name' => 'SideBar Nav menu',
            'url'  => '/product/sidebar-nav-menu.html',
            'children' => [
                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html',
                    'children' => [
                        [
                            'name' => 'SideBar Nav sub child menu',
                            'url'  => '/product/sidebar-nav-menu.html'
                        ],
                        
                        [
                            'name' => 'SideBar Nav sub child menu',
                            'url'  => '/product/sidebar-nav-menu.html'
                        ] 
                    ],
                ],
                
                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html'
                ],
                
                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html'
                ]
            ],
        ]
    ]
];
````

-------------------

### 必填参数：

`items`：多维数组 【导航数据，一个具有层级关系的数组，子级为children】

--------------------

### 选填参数：

`level`：数字类型, 默认值`5` 【导航层级】

`list_id`：数字类型, 默认值`digood-module-sidebar-item` 【导航列ID】

`wrap_id`：数字类型, 默认值`digood-module-sidebar-main` 【导航外包围ID】