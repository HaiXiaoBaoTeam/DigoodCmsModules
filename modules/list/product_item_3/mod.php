<?php
//+----------------------------------------------------|
// | Description: 列表产品模块 - 1
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-16
//+----------------------------------------------------|
## 验证必填参数
if(!isset($name) || !isset($img) || !isset($description) || !isset($url)) {
    echo "<code>参数不完整：需要传入 name, img, description, url</code>";
    return;
}
## 设置默认参数
if(!isset($button)) $button = 'View Detail';
?>


<div digood-id="list_product_item_3_oyh">
    <div class="single-project">
        <div class="image">
            <a class="fancybox" href="<?=$url?>" title="<?=$name?>"><img src="<?=$img?>" alt="<?=$name?>"></a>
        </div>
        <div class="text">
            <h4><a href="project-details.html" class="tran3s"><?=$name?></a></h4>
            <p><?=$description?></p>
            <!-- <span>House, Interior</span> -->
            <div class="button-box">
                <a href="<?=$url?>" class="link-button" role="button"><?=$button?></a>
            </div>
        </div> <!-- /.text -->
    </div> <!-- /.single-project -->
</div>