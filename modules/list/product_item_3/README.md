制作：2018年5月11日 OYH

### 数据结构演示

````
$data = [
    'name' => 'Dapibus ac facilisis in',
    'img' => 'http://placehold.it/500x350',
    'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.',
    'url' => '/product/item-1.html'
];
````

-------------------

### 必填参数：

`name`：字符串类型 【标题名称，建议限制长度】

`img`：字符串类型 【图片地址，需要绝对地址】

`description`：字符串类型 【描述，建议限制长度】

`url`：字符串类型 【URL地址，请填写相对地址：如'/product/item-1.html'】

--------------------

### 选填参数：

`button`：字符串, 默认值`View Detail` 【链接按钮名称】
