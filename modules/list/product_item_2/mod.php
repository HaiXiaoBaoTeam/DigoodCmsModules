<?php
//+----------------------------------------------------|
// | Description: 图文描述模块 - 1
// | 模块参数: name, img, description, url， button => 'View Detail'
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-15
//+----------------------------------------------------|
## 验证必填参数
if(!isset($name) || !isset($img) || !isset($description) || !isset($url)) {
    echo "<code>参数不完整：需要传入 name, img, description, url</code>";
    return;
}
## 设置默认参数
if(!isset($button)) $button = 'View Detail';
?>


<div class="panel panel-default"  digood-id="list_product_item_2_oyh">
    <div class="media panel-body">
        <div class="media-left">
            <a href="" class="media-link" title="<?=$name?>">
                <img class="media-object" src="<?=$img?>" alt="<?=$name?>">
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading"> 
                <a class="name-link" href="<?=$url?>"><?=$name?></a>    
            </h4>
            <p class="description"><?=$description?></p>
            <div class="button-box">
                <a href="<?=$url?>" class="btn btn-primary" role="button"><?=$button?></a>
            </div>
        </div>
    </div>
</div>