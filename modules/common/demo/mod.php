<?php
//+----------------------------------------------------|
// | Description: 示例
// | 模块参数: say
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-9
//+----------------------------------------------------|

## 验证必填参数
if(!isset($say)) {
    echo "<code>参数不完整：必须传入 say </code>";
    return;
}
## 设置默认参数
if(!isset($say_zh)) $say_zh = '你好，多谷CMS';
?>


<div digood-id="common_demo">
	<div class="en"><?=$say?><div>
	<div class="zh"><?=$say_zh?><div>
</div>