<?php
//+----------------------------------------------------|
// | Description: 热门产品模块
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-15
//+----------------------------------------------------|
## 验证必填参数
if(!isset($itemsData)) {
    echo "<code>参数不完整：需要传入 itemsData => array()</code>";
    return;
}

## 验证参数类型
if(!is_array($itemsData)) {
    echo "<code>参数错误：itemsData 应该为数组</code>";
    return;
}

## 设置默认参数
if(!isset($quantiy)) $quantiy = 4;
if(!isset($nav)) $nav = true;
?>





<div digood-id="common_hot_product_section_1_oyh">

    <div class="owl-carousel owl-theme" data-items="<?=$quantiy?>" data-nav="<?=intval($nav)?>">

        <?php foreach ($itemsData as $index => $item): ?>
            <?php
                ## 验证参数
                if(!isset($item['name']) || !isset($item['image']) || !isset($item['url']) || !isset($item['description'])) {
                    echo "<code>错误：请检查 itemsData 数据，数据中必须包含 name, image, url, description</code>";
                    return;
                }
                if(!isset($item['button'])) $item['button'] = 'View Detail';
            ?>
            <div class="item">
                <div class="thumbnail">
                    <a href="<?=$item['url']?>" title="<?=$item['name']?>">
                        <img class="image img-thumbnail" alt="<?=$item['name']?>" src="<?=$item['image']?>">
                    </a>
                    <div class="caption">
                        <h3 class="title"><?=$item['name']?></h3>
                        <p class="description"><?=$item['description']?></p>
                        <p class="buttons">
                            <a href="<?=$item['url']?>" title="<?=$item['name']?>" class="btn btn-primary" role="button"><?=$item['button']?></a>
                            <?php if(isset($item['sub_button_url']) && isset($item['sub_button'])): ?>
                            <a href="<?=$item['sub_button_url']?>" title="<?=$item['sub_button']?>" class="btn btn-default" role="button"><?=$item['sub_button']?></a>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
    
</div>