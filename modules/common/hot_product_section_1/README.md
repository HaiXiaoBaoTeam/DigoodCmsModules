制作：2018年5月11日 OYH

### 数据结构演示

````
$data = [
    'itemsData' => [
        [
            'name' => 'Dapibus ac facilisis in',
            'image' => 'http://placehold.it/500x350',
            'url' => '/product/all.html',
            'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.',
        ],
        [
            'name' => 'Dapibus ac facilisis in',
            'image' => 'http://placehold.it/500x350',
            'url' => '/product/all.html',
            'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.',
        ],
        [
            'name' => 'Dapibus ac facilisis in',
            'image' => 'http://placehold.it/500x350',
            'url' => '/product/all.html',
            'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.',
        ],
        [
            'name' => 'Dapibus ac facilisis in',
            'image' => 'http://placehold.it/500x350',
            'url' => '/product/all.html',
            'description' => 'Cras justo odio, dapi busac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.',
        ],
        ....
    ],

];
````

-------------------

### 必填参数：

`itemsData`：二维数组 【列表数据】

--------------------

### 选填参数：

`quantiy`：数字类型, 默认值`4` 【展示数量，建议不要超过5个】

`nav`：布尔类型, 默认值`true` 【是否显示切换按钮】