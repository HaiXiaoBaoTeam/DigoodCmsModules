<?php
//+----------------------------------------------------|
// | Description: 
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-16
//+----------------------------------------------------|
self::load('view/common/header');
?>



<?php $data = [
    'items' => [
        [
            'name' => 'SideBar Nav menu',
            'url'  => '/product/sidebar-nav-menu.html',
            'children' => [
                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html',
                    'children' => [
                        [
                            'name' => 'SideBar Nav sub child menu',
                            'url'  => '/product/sidebar-nav-menu.html'
                        ],

                        [
                            'name' => 'SideBar Nav sub child menu',
                            'url'  => '/product/sidebar-nav-menu.html'
                        ] 
                    ],
                ],

                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html'
                ],

                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html'
                ]
            ],
        ],
        [
            'name' => 'SideBar Nav menu',
            'url'  => '/product/sidebar-nav-menu.html',
            'children' => [
                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html',
                    'children' => [
                        [
                            'name' => 'SideBar Nav sub child menu',
                            'url'  => '/product/sidebar-nav-menu.html'
                        ],

                        [
                            'name' => 'SideBar Nav sub child menu',
                            'url'  => '/product/sidebar-nav-menu.html'
                        ] 
                    ],
                ],

                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html'
                ],

                [
                    'name' => 'SideBar Nav child menu',
                    'url'  => '/product/sidebar-nav-menu.html'
                ]
            ],
        ]
    ]
]; ?>

<div class="container">
    <div class="col-sm-12">
        <?php self::loadMod('sidebar/nav_1',$data); ?>
    </div>
</div>


<?php self::load('view/common/footer'); ?>