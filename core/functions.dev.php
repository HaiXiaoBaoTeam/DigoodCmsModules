<?php
//+----------------------------------------------------|
// | Description: 
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-16
//+----------------------------------------------------|

define('DG_FTP_PATH','');


class dev_self 
{
    

    private static $mods_style_arr = [];
    private static $mods_script_arr = [];
    private static $cache_assets_version  = 'v1';

    public static function loadMod($path, $param = null)
    {
        $mod_path = __DIR__."/../modules/{$path}/mod";
        $style_file = __DIR__."/../modules/{$path}/style.css";
        $script_file = __DIR__."/../modules/{$path}/script.js";
    
        if(!file_exists($mod_path.'.php')) {
            echo $path.'模块不存在';
            return;
        }
        if(file_exists($style_file) && !in_array(md5($style_file), self::$mods_style_arr, true)) self::useModStyle($style_file, $path);
        if(file_exists($script_file) && !in_array(md5($script_file), self::$mods_script_arr, true)) self::useModScript($script_file, $path);
    
        if(is_array($param)) {
            foreach ($param as $var => $value) {
                $$var = $value;
            }
        }
        include $mod_path.'.php';
    }



    
    public static function load($path, $param = null) 
    {
        $file = __DIR__."/../{$path}.php";
        if(is_array($param)) {
            foreach ($param as $var => $value) {
                $$var = $value;
            }
        }
        include $file;
    }


    public static function end()
    {
        
        self::initModAssets();
        self::initSetAssets();
    }

    
    public static function useModStyle($file, $name)
    {
        $begin = "\n\r\n\r/* ------------------  【{$name}】  BEGIN  ------------------- */\n\r";
        $end = "\n\r/* ------------------  【{$name}】  END  ------------------- */\n\r\n\r";
        ## 读取文件内容
        $content = file_get_contents($file);
        self::$mods_style_arr[md5($file)] = $begin.$content.$end;
    }



    public static function useModScript($file, $name)
    {
        $begin = "\n\r\n\r/* ------------------  【{$name}】  BEGIN  ------------------- */\n\r(function(){\n\r";
        $end = "\n\r}())\n\r/* ------------------  【{$name}】  END  ------------------- */\n\r\n\r";
        ## 读取文件内容
        $content = file_get_contents($file);
        self::$mods_script_arr[md5($file)] = $begin.$content.$end;
    }



    
    public static function outModStyle()
    {
        
        $page = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : 'index';
        $page = str_replace('.php','',$page);
        $page = trim($page,'/');
        if($page) {
            $page = explode('/',$page);
        } else {
            $page =  ['index'];
        }
        $page = $page[0];
        $path = '/cache/digood_modules_'.$page.'_'.self::$cache_assets_version.'.css';
        echo '<link rel="stylesheet" href="'.$path.'">';
    }



    public static function outModScript()
    {
        
        $page = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : 'index';
        $page = str_replace('.php','',$page);
        $page = trim($page,'/');
        if($page) {
            $page = explode('/',$page);
        } else {
            $page =  ['index'];
        }
        $page = $page[0];
        $path = '/cache/digood_modules_'.$page.'_'.self::$cache_assets_version.'.js';
        echo '<script src="'.$path.'"></script>';
    }


    
    public static function initModAssets() {
        
        $style_str = '';
        $script_str = '';
        
        ## 样式
        foreach (self::$mods_style_arr as $key => $value) {
            $style_str .= $value;
        }

        ## 脚本
        foreach (self::$mods_script_arr as $key => $value) {
            $script_str .= $value;
        }
        
        $page = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : 'index';
        $page = str_replace('.php','',$page);
        $page = trim($page,'/');
        if($page) {
            $page = explode('/',$page);
        } else {
            $page =  ['index'];
        }
        $page = $page[0];
        ## 文件
        $style_file = __DIR__.'/../cache/digood_modules_'.$page.'_'.self::$cache_assets_version.'.css';
        $script_file = __DIR__.'/../cache/digood_modules_'.$page.'_'.self::$cache_assets_version.'.js';

        
        $file = fopen($style_file, 'w');
        fwrite($file, $style_str);
        fclose($file);

        $file = fopen($script_file, 'w');
        fwrite($file, $script_str);
        fclose($file);
    }
    


    private static $set_style_arr = [];
    private static $set_script_arr = [];
    public static function setStyle($code, $name='')
    {
        $begin = "\n\r\n\r/* ------------------ {$name}  BEGIN  ------------------- */\n\r";
        $end = "\n\r/* ------------------ {$name}  END  ------------------- */\n\r\n\r";
        self::$set_style_arr[] = $begin.$code.$end;
    }




    public static function setScript($code, $name='')
    {
        $begin = "\n\r\n\r/* ------------------  【{$name}】  BEGIN  ------------------- */\n\r(function(){\n\r";
        $end = "\n\r})();\n\r/* ------------------  【{$name}】  END  ------------------- */\n\r\n\r";
        self::$set_script_arr[] = $begin.$code.$end;
    }




    public static function outStyle()
    {
        
        $page = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : 'index';
        $page = str_replace('.php','',$page);
        $page = trim($page,'/');
        if($page) {
            $page = explode('/',$page);
        } else {
            $page =  ['index'];
        }
        $page = $page[0];
        $name = 'digood_'.$page.'_page_'.self::$cache_assets_version.'.css';
        $path = '/cache/'.$name.'?'.time();
        if(file_exists(__DIR__.'/../cache/'.$name)) echo '<link rel="stylesheet" href="'.$path.'">';
    }




    public static function outScript()
    {
        
        $page = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : 'index';
        $page = str_replace('.php','',$page);
        $page = trim($page,'/');
        if($page) {
            $page = explode('/',$page);
        } else {
            $page =  ['index'];
        }
        $page = $page[0];
        $name = 'digood_'.$page.'_page_'.self::$cache_assets_version.'.js';
        $path = '/cache/'.$name.'?'.time();
        if(file_exists(__DIR__.'/../cache/'.$name)) echo '<script src="'.$path.'"></script>';
    }


    
    
    public static function initSetAssets() {
        
        $style_str = '';
        $script_str = '';
        
        ## 样式
        foreach (self::$set_style_arr as $key => $value) {
            $style_str .= $value;
        }

        ## 脚本
        foreach (self::$set_script_arr as $key => $value) {
            $script_str .= $value;
        }
        
        $page = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : 'index';
        $page = str_replace('.php','',$page);
        $page = trim($page,'/');
        if($page) {
            $page = explode('/',$page);
        } else {
            $page =  ['index'];
        }
        $page = $page[0];
        ## 文件
        $style_file = __DIR__.'/../cache/digood_'.$page.'_page_'.self::$cache_assets_version.'.css';
        $script_file = __DIR__.'/../cache/digood_'.$page.'_page_'.self::$cache_assets_version.'.js';

        $file = fopen($style_file, 'w');
        fwrite($file, $style_str);
        fclose($file);

        $file = fopen($script_file, 'w');
        fwrite($file, $script_str);
        fclose($file);
    }



}


function Lang($string) {
    return $string;
}
