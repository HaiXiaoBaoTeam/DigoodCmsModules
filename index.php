<?php
//+----------------------------------------------------|
// | Description: 
// +---------------------------------------------------|
// | Author: 浩丶IMOYH [oyhemail@163.com]
// | Last Modified: 2018-5-14
//+----------------------------------------------------|
include_once 'core/functions.dev.php';
$DG_TOOL = new dev_self();

$path = '';
if(isset($_SERVER['PATH_INFO'])) {
	$path = trim($_SERVER['PATH_INFO'],'/');
}

if($path == '') $path = 'index';
$file = __DIR__."/view/{$path}.php";
if(!file_exists($file)) exit("/view/{$path}.php 不存在");
$DG_TOOL->load("view/{$path}");
$DG_TOOL->end();






